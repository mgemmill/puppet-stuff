class python {

  package {'python2.7':
    name   => 'libpython2.7',
    ensure => installed,
    before => Notify['python2.7 installed'],
  }

  notify {'python2.7 installed':
    message => 'python 2.7 has been installed.',
  }

  package {'python-setuptools':
    ensure  => installed,
    before  => Notify['setuptools installed'],
    require => Package['python2.7'],
  }

  notify {'setuptools installed':
    message => 'python-setuptools has been installed.',
  }

  package {'python-pip':
    ensure  => installed,
    before  => Notify['pip installed'],
    require => Package['python2.7', 'python-setuptools'],
  }

  notify {'pip installed':
    message => 'python-pip has been installed.',
  }

  define install_pypkg($pypkg_name, $module_to_test_import) {
    exec {
      "InstallPkg_$pypkg_name":
      command => "/usr/bin/pip install ${pypkg_name}",
      unless  => "/usr/bin/python2.7 -c 'import ${module_to_test_import}'",
      require => Package['python-pip'],
    }
  }

  install_pypkg {
    "virtualenv":
    pypkg_name               => "virtualenv",
    module_to_test_import => "virtualenv";

    "yolk":
    pypkg_name               => "yolk",
    module_to_test_import => "yolk";
  }
}
