include common

class ubuntu {

  /*
  define install_pkg($pkgname) {
    notify {"$pkgname installed":
      message => "  ====>   `$pkgname` has been installed.", 
    }
    package {"$pkgname":
      ensure => installed,
      before => Notify["$pkgname installed"],
    }
  }
  */

  #
  # build-essential covers gcc and make on debian systems
  #
  class { 'common::install_pkg': 
    pkgname => 'build-essential'; }

/*
    pkgname => 'git-core';
    pkgname => 'tmux';
  }
  */

  # 
  # secondary packages (maybe don't really need)
  #
  # install_pkg {'zlib1g-dev': pkgname => 'zlib1g-dev' }
  # install_pkg {'sqlite3': pkgname => 'sqlite3' }

}
