# postgres/manifests/init.pp

class postgres {
  package {"postgresql-9.1":
    ensure => installed
  }
  user { 'postgres-user':
    name => 'postgres',
    ensure => present,
    require => Package['postgresql-9.1']
  }
  service {'postgres-service':
    name => 'postgres',
    ensure => running,
    require => Package['postgresql-9.1']
  }
}

/*
# Initialize the database with the password password.
define postgres::initdb(password => "") { 
  exec { "InitDB":

    command => "/bin/chown postgres.postgres /var/lib/pgsql && echo \"${password}\" > /tmp/ps && /bin/su  postgres -c \"/usr/bin/initdb /var/lib/pgsql/data --auth='password' --pwfile=/tmp/ps -E UTF8 \" && rm -rf /tmp/ps",
  require =>  [User['postgres'], Package["postgresql-${version}"]],
  unless  => "/usr/bin/test -e /var/lib/pgsql/data/PG_VERSION ",
  }
}

# Start the service if not running
define postgres::enable {
  service { postgresql:
  ensure    => running,
  enable    => true,
  hasstatus => true,
  require   => Exec["InitDB"],
}
}


# Postgres host based authentication
define postgres::hba ($password="",$allowedrules){
  file { "/var/lib/pgsql/data/pg_hba.conf":
    content   => template("postgres/pg_hba.conf.erb"),
    owner     => "root",
    group     => "root",
    notify    => Service["postgresql"],
    # require => File["/var/lib/pgsql/.order"],
    require   => Exec["InitDB"],
  }
}

define postgres::config ($listen="localhost")  {
  file {"/var/lib/pgsql/data/postgresql.conf":
    content => template("postgres/postgresql.conf.erb"),
    owner   => postgres,
    group   => postgres,
    notify  => Service["postgresql"],
    # require => File["/var/lib/pgsql/.order"],
    require => Exec["InitDB"],
  }
}

# Base SQL exec
define sqlexec($username, $password, $database, $sql, $sqlcheck) {
  if $password == "" {
    exec{ "psql -h localhost --username=${username} $database -c \"${sql}\" >> /var/lib/puppet/log/postgresql.sql.log 2>&1 && /bin/sleep 5":
      path        => $path,
      timeout     => 600,
      unless      => "psql -U $username $database -c $sqlcheck",
      require     =>  [User['postgres'],Service[postgresql]],
    }
  } else {
    exec{ "psql -h localhost --username=${username} $database -c \"${sql}\" >> /var/lib/puppet/log/postgresql.sql.log 2>&1 && /bin/sleep 5":
      environment => "PGPASSWORD=${password}",
      path        => $path,
      timeout     => 600,
      unless      => "psql -U $username $database -c $sqlcheck",
      require     =>  [User['postgres'],Service[postgresql]],
    }
  }
}

# Create a Postgres user
define postgres::createuser($passwd) {
  sqlexec{ createuser:
    password => $password,
    username => "postgres",
    database => "postgres",
    sql      => "CREATE ROLE ${name} WITH LOGIN PASSWORD '${passwd}';",
    sqlcheck => "\"SELECT usename FROM pg_user WHERE usename = '${name}'\" | grep ${name}",
    require  =>  Service[postgresql],
  }
}

# Create a Postgres db
define postgres::createdb($owner) {
  sqlexec{ $name:
    password => $password,
    username => "postgres",
    database => "postgres",
    sql      => "CREATE DATABASE $name WITH OWNER = $owner ENCODING = 'UTF8';",
    sqlcheck => "\"SELECT datname FROM pg_database WHERE datname ='$name'\" | grep $name",
    require  => Service[postgresql],
  }
}
*/
