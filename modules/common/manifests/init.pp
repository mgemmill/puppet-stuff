class common {
}

define common::install_pkg($pkgname) {
  notify {"$pkgname installed":
    message => "  ====>   `$pkgname` has been installed.", 
  }
  package {"$pkgname":
    ensure => installed,
    before => Notify["$pkgname installed"],
  }
}
