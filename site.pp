node default {
  include ubuntu
  include python


  /*include postgres*/

  /*
  class {'postgres': version => '9.1' }

  postgres::initdb{ "host": }
  postgres::config{ "host": listen => "*", }
  postgres::hba{ "host":
    allowedrules => [
      "host   test all  10.0.0.0/32 trust",
    ],
  }
  postgres::enable{ "host": }
  postgres::createuser{ "mark": passwd => "password", }
  postgres::createdb{ "test": owner => "postgres", }
  */


}
