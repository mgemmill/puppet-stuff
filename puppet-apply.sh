#!/bin/bash
# run puppet from this directory
HERE=`pwd`
PARENT=`dirname $HERE` 
MODULES="$HERE/modules:$PARENT/puppet-modules"
SITE="$HERE/site.pp"

sudo puppet apply --modulepath $MODULES $SITE 

